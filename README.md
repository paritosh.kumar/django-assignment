# Django Project - Company Asset Management System

Company Asset Management System made using Django. Prototype 
<!-- (link)[https://xd.adobe.com/view/f33e3d64-d6bd-4b8a-6ccb-80067ce04cde-850f/]  -->

<!-- 
- ### Creating a virutal environment and installing Django
    - Create a virutal environment using ```python3 -m venv virtualenv``` command.
    - Activate the virtual environment using ```myvenv\Scripts\activate```.

    The virtual environment helps us keep our python dependencies seperate. The local machine might have any version of python installed but our virutalenv will make use of a particular python version.  

    - After activating the ```virtualenv```, make sure that pip is up to date using the ```python -m pip install --upgrade pip``` command.

    - Install django using ```python -m pip install Django```.

- ### Setting up django project

    - To setup the django project run the command ```django-admin startproject <PROJECT_NAME>```. This will setup the django project with all the required directories and files. Mine is ```asset_manager```.

    - Django comes with a built in python server. We run it using ```python manage.py runserver```. ```Cd``` into the project folder where in the main manage.py file lies and run the command.

    - Now that the project is setup, we will create an django <b>app</b> inside the main project directory. Use the command ```python manage.py startapp <APP_NAME>``` for creating an app. Use a different name from the project as this will create an error.
            Note: The difference between <b>apps and proejct</b>
                An app is a Web application that does something – e.g., a Weblog system, a database of public records or a small poll app. A project is a collection of configuration and apps for a particular website. A project can contain multiple apps. An app can be in multiple projects.
    - 


## Todo checklist 
- [ ] Create user flow (UX).
- [ ]  -->