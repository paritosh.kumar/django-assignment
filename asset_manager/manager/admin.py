from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
# from django.contrib.auth.models import Group
# Register your models here.
from .models import Asset
from .models import Employee
from .models import Assignment
# from .models import CustomUser

admin.site.register(Asset)
admin.site.register(Employee)
admin.site.register(Assignment)
# admin.site.register(CustomUser, UserAdmin)
