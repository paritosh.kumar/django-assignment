# Generated by Django 3.1 on 2020-08-26 16:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0003_assignments'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignments',
            name='duration',
            field=models.DateTimeField(),
        ),
    ]
