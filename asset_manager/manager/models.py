from django.db import models
# from django.contrib.auth.models import (
#     BaseUserManager, AbstractUser
# )
from django.contrib.auth.models import User
# Create your models here.

class Asset(models.Model):
    ASSET_CHOISES = [
        ('LAPTOP','Laptop'),
        ('LAPTOP_BATTERY','Laptop Battery'),
        ('PEN','Pen'),
        ('MOBILE','Mobile')
    ]
    asset_type = models.CharField(max_length=150,choices=ASSET_CHOISES)
    description = models.CharField(max_length=300,blank=True)
    assigned_status = models.BooleanField(default=False)
 
    

    def __str__(self):
        return self.asset_type + " " + str(self.id)



class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    activated = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class Assignment(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE) 
    asset = models.OneToOneField(Asset, on_delete=models.CASCADE)
    request_date = models.DateTimeField(auto_now_add=True)
    duration = models.DateField(auto_now=False)
    ASSIGNMENT_STATUS_CHOISES = [
        ('Pending','Pending'),
        ('Rejected','Rejected'),
        ('Approved','Approved')
    ]
    status = models.CharField(
        choices=ASSIGNMENT_STATUS_CHOISES,
        max_length=100,
        default='Pending'
    )

    def __str__(self):
        return self.employee.user.username + ' : ' + self.asset.asset_type + ' : ' + self.status

    def save(self, *args, **kwargs):
        if self.status == 'Approved':
            assetN = Asset.objects.get(id = self.asset.id)
            assetN.assigned_status = True
            assetN.save()
            super(Assignment, self).save(*args, **kwargs)

        super(Assignment, self).save(*args, **kwargs)    

