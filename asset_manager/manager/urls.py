from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import AuthenticationForm

urlpatterns = [
    path('', views.index, name='index'),
    path('signup', views.signup, name='signup'),
    path('employee/login', auth_views.LoginView.as_view(), name='employee.login'),
    path('employee/logout', auth_views.LogoutView.as_view(), name='employee.logout'),
    path('employee/asset-invetory', views.asset_inventory, name='employee.asset_inventory'),
    path('employee/asset-request', views.available_assets, name='available_assets'),
    path('employee/asset-request/<asset_type>', views.request_asset, name='request_asset'),


]