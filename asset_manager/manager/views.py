from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
# Create your views here.
from .forms import SignUpForm
from .forms import AssetRequestForm
from django.contrib.auth import login, authenticate
from .models import Employee
from .models import Assignment
from .models import User
from .models import Asset

from django.core.mail import send_mail

from django.contrib.auth.decorators import login_required


def index(request):
    template = loader.get_template('manager/index.html')
    return render(request,'manager/index.html')

def admin_login(request):
    return HttpResponse('Admin login page')

def admin_dashboard(request):
    return HttpResponse('admin dashbaord')

def asset_page(request):
    return HttpResponse('asset')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            employee = Employee(user = user)
            employee.save()
            send_mail(
    'Subject here',
    'Here is the message.',
    'django.paritosh.kumar@gmail.com',
    ['kumar.paritosh20@gmail.com'],
    fail_silently=False,
    
)
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'manager/signup.html', {'form': form})

def login_employee(request):
    return render(request,'manager/employees/login.html')


@login_required
def available_assets(request):
    available_assets = set()
    assets = Asset.objects.filter(assigned_status=False)
    for asset in assets:
        available_assets.add(asset.asset_type)
    
    return render(request, 'manager/assets_summary.html',{'assets':available_assets})     

@login_required
def request_asset(request,asset_type):

    asset = Asset.objects.filter(asset_type=asset_type,assigned_status = False)   
    # this will create a protected route for assets that are unavaible 
    if not asset:
        return redirect('available_assets')
    user = User.objects.get( username = request.user.username)
    employee = Employee.objects.get( user = user)
    if request.method == 'POST':
        
        form = AssetRequestForm(request.POST)    
        if form.is_valid():
            assignment = form.save()
            assignment.employee = employee
            assignment.asset = asset[0] 
            assignment.save()
            send_mail(
    'Asset Reques ',
    'Here is the message.',
    'django.paritosh.kumar@gmail.com',
    ['kumar.paritosh20@gmail.com'],
    fail_silently=False,
    
)
        return redirect('available_assets')
    else:
        form = AssetRequestForm()
    return render(request, 'manager/asset_request.html', {'form': form})    
    


 

@login_required
def asset_inventory(request):
    user = User.objects.get( username = request.user.username)
    employee = Employee.objects.get( user = user)
    employee_asset_assignments = Assignment.objects.filter( employee = employee)
    return render(request,'manager/asset_inventory.html',{'assets':employee_asset_assignments})